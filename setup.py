"""
Contains the setup script for the DudeGL project.
Author: jven@mit.edu (Justin Venezuela)
"""

from setuptools import find_packages
from setuptools import setup

def main():
  setup(
      name = 'DudeGL',
      version = '0.1',
      packages = find_packages(),
      install_requires = [
          'PyOpenGL>=3.0.2'])
if __name__ == '__main__':
  main()
